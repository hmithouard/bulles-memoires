package com.example.projet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/* Paramètres, scores, ui*/
public class Jeu extends AppCompatActivity implements View.OnTouchListener {
    private MyView myView;
    private TextView Tpatouche;
    private int temps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);
        Tpatouche = findViewById(R.id.patouche);
        getExtra();
        myView = new MyView(this,this.temps);
        setContentView(myView);
        myView.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            int x = (int) event.getX();
            int y = (int) event.getY();
            if(!myView.getPatouche()) myView.update(x,y);
        }
        return false;
    }
    public void getExtra(){
        this.temps = 10;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if(extras.containsKey("TEMPS"))temps = extras.getInt("TEMPS");
        }
    }

    public class MyView extends View
    {
        private int x;
        private int y;
        private int radius;
        private Cercle tabC[];
        private int sizeTabC;
        private int nbEllements;
        private int tmpMax; // nombre max de cercles dans le niveau actuel (peut augmenter)
        private int route;  // numéro du cercle actuel à cliquer
        private Random rand;
        private Boolean patouche;   // variable pour savoir si l'utilisateur peut appuyer sur l'écrant
        Paint paint = null;
        DisplayMetrics metrics;

        private int score;  // score actuel
        private int level; // niveau actuel

        private int interval = 10000; // 10 Second

        // handler pour figer l'écrant pendant un interval
        private Handler handler = new Handler();
        private Runnable runnable = new Runnable(){
            public void run() {
                Tpatouche.setText("false");
                invalidate();
            }
        };
        public MyView(Context context,int temps)
        {
            super(context);
            metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            Resources res = getResources();

            rand = new Random();

            interval = temps * 1000;

            tabC = new Cercle[8];
            sizeTabC = 8;
            nbEllements =0;
            tmpMax = 3;
            route = 0;

            score = 0;
            level = 1;

            checkEtatPatouche();

            genCercles();

            paint = new Paint();
        }
        //Génération des cercles
        public void genCercles(){
            int x,y,rad,i;
            for( i= 0; i<this.tmpMax;i++){
                rad = rand.nextInt(250 - 110 + 1) + 110;
                x = rand.nextInt((metrics.widthPixels - rad) - rad + 1) + rad;
                y = rand.nextInt((metrics.heightPixels - rad) - rad + 1) + rad;
                if(isPosOk(x,y,rad)) {
                    this.tabC[i] = new Cercle(x, y, rad);
                    this.nbEllements++;
                }
                else i--;
            }
        }
        //Check si la position aléatoire du cerlce ne chevauche aucun autres
        public boolean isPosOk(int x,int y,int rad){
            for(int i = 0; i<this.nbEllements;i ++) {
                //int distancesquared = (x - this.tabC[i].getX()) * (x - this.tabC[i].getX()) + (y - this.tabC[i].getY()) * (y - this.tabC[i].getY());
                //if (distancesquared <= radius * radius)return false;
                if(x<= this.tabC[i].getX()+this.tabC[i].getRadius() + 200 && x >= this.tabC[i].getX()-this.tabC[i].getRadius() - 200){
                    if(y<= (this.tabC[i].getY()+this.tabC[i].getRadius()+ 200) && y>= this.tabC[i].getY()-this.tabC[i].getRadius() - 200){
                        return false;
                    }
                }
            }
            return true;
        }
        //Update des états, supprime les cerlces touché et passe au niveau suivant si il n'y a plus de cercles
        public void update(int x,int y){
                boolean j = true;
                    for (int i = 0; i < sizeTabC; i++) {
                        if (this.tabC[i] == null) continue;
                        if (isIn(x, y, i)) {
                            if (i != route) {
                                j = false;
                                Tpatouche.setText("true");
                                this.tmpMax = 3;
                                nbEllements = 0;
                                route = 0;
                                startIntent();

                            }
                            if (j) {
                                tabC[i] = null;
                                nbEllements--;
                                route++;
                                score += 10;
                                if (nbEllements == 0) {
                                    if (tmpMax < sizeTabC) {
                                        this.tmpMax++;
                                        this.score += 100;
                                        this.level++;
                                    }
                                    Tpatouche.setText("true");
                                    route = 0;
                                    this.genCercles();
                                }
                                invalidate();
                            }
                        }
                    }
        }
        //Check si le click est dans un cercle
        public boolean isIn(int x,int y,int i){
            int distancesquared = (x - this.tabC[i].getX()) * (x - this.tabC[i].getX()) + (y - this.tabC[i].getY()) * (y - this.tabC[i].getY());
            return distancesquared <= radius * radius;
        }
        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);
            checkEtatPatouche();
            radius = 100;
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.WHITE);
            canvas.drawPaint(paint);
                for (int i = 0; i < this.tmpMax; i++) {
                    if (tabC[i] != null) {
                        String s = String.valueOf(i + 1);
                        String sc = "Score : "+this.score;
                        paint.setColor(Color.parseColor(this.tabC[i].getColor()));
                        canvas.drawCircle(this.tabC[i].getX(), this.tabC[i].getY(), this.tabC[i].getRadius(), paint);

                        paint.setColor(Color.BLACK);
                        paint.setTextSize(70);
                        canvas.drawText(sc, (metrics.widthPixels/2)-(paint.measureText(sc)/2), 80, paint);

                        if (this.patouche) {
                            paint.setColor(Color.WHITE);
                            paint.setTextSize(100);
                            canvas.drawText(s, this.tabC[i].getX() - 25, this.tabC[i].getY() + 25, paint);
                        }
                    }
                }


                if (this.patouche) handler.postDelayed(runnable, interval);
        }
        //Check de la variable global "patouche" pour savoir si l'utilisateur peut cliquer sur l'écrant
        public void checkEtatPatouche(){
            if(Tpatouche.getText().equals("true")) patouche = true;
            else patouche = false;
        }
        public void startIntent(){
            Intent jeuIntent = new Intent(Jeu.this,MainActivity.class);
            jeuIntent.putExtra("SCORE",this.score);
            jeuIntent.putExtra("LEVEL",this.level);
            startActivity(jeuIntent);
        }
        public Boolean getPatouche() {
            return patouche;
        }
    }
}