package com.example.projet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Settings extends AppCompatActivity implements View.OnClickListener {
    private EditText tempsReflexion;
    private Button valider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        valider = findViewById(R.id.btnValider);
        tempsReflexion = findViewById(R.id.editTextTemps);

        valider.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == valider){
            if(isInteger(this.tempsReflexion.getText().toString())){
                int temps = Integer.parseInt(this.tempsReflexion.getText().toString());
                Intent settingsIntent = new Intent(Settings.this,MainActivity.class);
                settingsIntent.putExtra("TEMPS",temps);
                startActivity(settingsIntent);
            }else{
                Toast.makeText(this,"Veuillez entrer un chiffre valide !",Toast.LENGTH_SHORT).show();
            }
        }
    }
    //Fonction pour tester si la String est un int valide
    // source : https://stackoverflow.com/questions/237159/whats-the-best-way-to-check-if-a-string-represents-an-integer-in-java
    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }
}