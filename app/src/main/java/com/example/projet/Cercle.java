package com.example.projet;

import java.util.Random;

public class Cercle {
    private int x,y,radius;
    private Random rand;
    private String color;
    private String[] colors = {"#CD5C5C","#f7b56f","#5ccd6b","#5ccdcd","#875ccd","#c1cd5c"};
    public Cercle(){
        this.x = 0;
        this.y = 0;
        this.radius = 10;
        this.color = this.colors[0];
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Cercle(int x, int y, int radius){
        rand = new Random();
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = this.colors[rand.nextInt(5)];
    }
    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }
}
