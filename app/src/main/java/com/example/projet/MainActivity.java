package com.example.projet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String MY_BACKUP_FILE = "MY_BACKUP_FILE";
    private static final String MY_BACKUP_FILE_SCORING = "MY_BACKUP_FILE_SCORING";
    private static final String MY_BACKUP_FILE_USERS = "MY_BACKUP_FILE_USERS";
    private static final String MY_BACKUP_FILE_CURRENT_USER = "MY_BACKUP_FILE_CURRENT_USER";
    private Button btnJouer,btnSettings;
    private TextView viewScore;
    private int score,level,temps;
    private EditText userName;
    private String[] users = {"Joueur","Joueur","Joueur","Joueur","Joueur","Joueur","Joueur","Joueur","Joueur","Joueur"};
    private int[] scores = {10,9,8,7,6,5,4,3,2,1};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnJouer = findViewById(R.id.btnJouer);
        btnSettings = findViewById(R.id.btnSettings);
        viewScore = findViewById(R.id.textViewScores);
        userName = findViewById(R.id.editTextUserName);

        btnJouer.setOnClickListener(this);
        btnSettings.setOnClickListener(this);

        temps = 10;
        score = 0;
        level = 1;

        userName.setText(getSharedPreferences(MY_BACKUP_FILE,MODE_PRIVATE).getString(MY_BACKUP_FILE_CURRENT_USER,"Joueur"));

        getExtra();
        update_score_tab();
        saveUser();
    }
    @Override
    public void onClick(View view){
        if(view == btnJouer){
            Intent mainActivityIntent = new Intent(MainActivity.this,Jeu.class);
            mainActivityIntent.putExtra("TEMPS",this.temps);
            getSharedPreferences(MY_BACKUP_FILE,MODE_PRIVATE)
                    .edit()
                    .putString(MY_BACKUP_FILE_CURRENT_USER,this.userName.getText().toString())
                    .apply();
            startActivity(mainActivityIntent);
        }
        if(view == btnSettings){
            Intent mainActivityIntent = new Intent(MainActivity.this,Settings.class);
            getSharedPreferences(MY_BACKUP_FILE,MODE_PRIVATE)
                    .edit()
                    .putString(MY_BACKUP_FILE_CURRENT_USER,this.userName.getText().toString())
                    .apply();
            startActivity(mainActivityIntent);
        }
    }
    private void getExtra() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if(extras.containsKey("SCORE"))score = extras.getInt("SCORE");
            if(extras.containsKey("LEVEL"))level = extras.getInt("LEVEL");
            if(extras.containsKey("TEMPS"))temps = extras.getInt("TEMPS");
        }
    }
    // sauvegarde du tableau des 10 utilisateurs avec le plus grand score ( pseudo et score )
    private void saveUser(){
        StringBuilder str = new StringBuilder();
        StringBuilder str_name = new StringBuilder();
        for (int i = 0; i < scores.length; i++) {
            str.append(scores[i]).append(",");
            str_name.append(users[i]).append(",");
        }
        getSharedPreferences(MY_BACKUP_FILE,MODE_PRIVATE)
                .edit()
                .putString(MY_BACKUP_FILE_SCORING,str.toString())
                .putString(MY_BACKUP_FILE_USERS,str_name.toString())
                .apply();
    }

    // update du tableau des scores
    private void update_score_tab(){
        String savedString = getSharedPreferences(MY_BACKUP_FILE,MODE_PRIVATE).getString(MY_BACKUP_FILE_SCORING,",");
        String savedString_u = getSharedPreferences(MY_BACKUP_FILE,MODE_PRIVATE).getString(MY_BACKUP_FILE_USERS,",");
        StringTokenizer st = new StringTokenizer(savedString, ",");
        StringTokenizer st_u = new StringTokenizer(savedString_u, ",");
        for (int i = 0; i < 9; i++) {
            scores[i] = Integer.parseInt(st.nextToken());
            users[i] = st_u.nextToken();
        }
        checkNewScore();

    }

    // supprime les ancients scores si le nouveau est meilleur
    private void checkNewScore(){
        int tmp,s;
        String t,u,tmp_s;
        s = this.score;
        u = this.userName.getText().toString();
        for(int i=0; i< scores.length; i++){
            if(s> scores[i]){
                tmp = scores[i];
                scores[i] = s;
                s = tmp;
                tmp_s = users[i];
                users[i] = u;
                u = tmp_s;
            }
            t = this.viewScore.getText()+users[i]+"\t"+String.valueOf(scores[i])+"\n";
            this.viewScore.setText(t);
        }
    }
        }